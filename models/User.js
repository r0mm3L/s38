const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    email: {
        type: String,
        required: [true, 'Email is required']

    },
    password: {
        type: String,
        required: [true, 'Password is required']
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    orders: [
        {
            userId: {
                type: String,
                required: false
            },
            productId: {
                type: String,
                required: [true, 'Product ID is required']
            },
            totalAmount: {
                type: Number,
            },
            purchasedOn: {
                type: Date,
                default: new Date()
            }

        

        }
    ]


})

module.exports = mongoose.model('User', userSchema)