const express = require("express");
const router = express.Router();
const userController = require("../controllers/user")
const auth = require("../auth");

// REGISTRATION
router.post("/register", (req, res) => {
    userController.registerUser(req.body).then(resultFromController => 
        res.send(resultFromController))
})

// LOGIN
router.post("/login", (req, res) => {
    userController.loginUser(req.body).then(resultFromController => 
        res.send(resultFromController));
})

// SET USER AS ADMIN

router.put('/:id/setasadmin', auth.verify,(req, res) => {

 

    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin === true) {
    userController.setUserAsAdmin(req.params.id, req.body).then(resultFromController => 
        res.send(resultFromController));
    } else {
		res.send("Unauthorized");
	}	
});


// USER CHECK OUT - CREATE ORDER
router.post("/checkout", auth.verify,(req, res) => {

    const userData = auth.decode(req.headers.authorization);

	let data = {

        userId: userData.id, 
		productId: req.body.productId,
        price: req.body.price
	}

    if(userData.isAdmin === false) {

	userController.orderProduct(data).then(resultFromController => 
		res.send(resultFromController));
    } else {
		res.send("Unauthorized");
	}	

});

// RETRIEVE AUTHENTICATED USER'S ORDERS
router.get("/myorders", auth.verify, (req,res) => {

    const userData = auth.decode(req.headers.authorization);

	let data = {

        orders: userData.orders
	}
    if(userData.isAdmin === false) {
    userController.getMyOrders(data).then(resultFromController =>
       res.send(resultFromController));
    } else {
		res.send("Unauthorized");
	}
})

// RETRIEVE ALL ORDERS BY ADMIN

router.get("/orders", auth.verify,(req, res) => {

    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin === true) {
    userController.getOrders().then(resultFromController => 
        res.send(resultFromController))
    } else {
		res.send("Unauthorized");
	}
})

module.exports = router;