const express = require("express");
const router = express.Router();
const productController = require("../controllers/product")
const auth = require("../auth");

// Retrieve ALL ACTIVE PRODUCTS
router.get("/activeproducts", (req,res) => {
    productController.getAllActive().then(resultFromController =>
       res.send(resultFromController))
})

  // Retreive SINGLE PRODUCT
router.get("/:productId", (req,res) => {
    productController.getProduct(req.params).then(resultFromController =>
        res.send(resultFromController))
})

// CREATE PRODUCT
router.post("/createproduct", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin === true) {
    productController.createProduct(req.body).then(resultFromController =>
    res.send(resultFromController));

    } else {
    res.send("Unauthorized");
    }	

})

// UPDATE A PRODUCT
router.put("/:productId", auth.verify, (req,res) => {
    
    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin === true) {
    productController.updateProduct(req.params, req.body).then(resultFromController =>
        res.send(resultFromController))
    } else {
        res.send("Unauthorized");
        }	
})

// ARCHIVE PRODUCT
router.put('/:id/archive', auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin === true) {
    productController.archiveProduct(req.params.id, req.body).then(resultFromController => 
        res.send(resultFromController));
    } else {
		res.send("Unauthorized");
	}	
});



module.exports = router;

