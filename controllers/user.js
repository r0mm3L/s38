const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

//REGISTRATION
module.exports.registerUser = (reqBody) => {
    let newUser = new User({
        email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 10)

    })

    return newUser.save().then((user, error) => {
        if(error) {
            return false

        } else {
            return true
        }
    })
}

// LOGIN 
module.exports.loginUser = (async reqBody => {
    const result = await User.findOne({ email: reqBody.email });
    if (result == null) {
        return false;
    } else {
        const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

        if (isPasswordCorrect) {
            return { access: auth.createAccessToken(result) };
        } else {
            return false;
        }
    }
})

// SET USER AS ADMIN
module.exports.setUserAsAdmin = (userId, newBody) => {
    return User.findById(userId).then((result) => {


        result.isAdmin = newBody.isAdmin
        return result.save().then((newAdmin, sendErr) => {

            if(sendErr){
                console.log(sendErr)
                return false
            } else {
                return newAdmin
            }
        })
    })
}



// USER CHECK OUT - CREATE ORDER
module.exports.orderProduct = async (data) => {

   
        let isUserUpdated = await User.findById(data.userId).then( user =>{
    
            user.orders.push({productId: data.productId, userId: data.userId, totalAmount: data.price});
    
            return user.save().then((user, error) => {
                if(error) {
                    return false
                } else {
                    return true
                }
            })
        })
    
                if(isUserUpdated) {
                return true
            } 
    
  
            else {
                return false
            }
    
    }

    // RETRIEVE AUTHENTICATED USER'S ORDERS
module.exports.getMyOrders = (data) => {
    return User.findOne(data).then(result =>{

        let result2 = {
            orders: result.orders
        }
        return result2;
    })
}

// RETRIEVE ALL ORDERS BY ADMIN

// module.exports.getOrders = (data) => {
    module.exports.getOrders = () => {
    return User.find({},{orders:1}).then(result => {

      return result
    })


}